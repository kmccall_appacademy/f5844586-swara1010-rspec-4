class Timer

  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    mins, secs = seconds.divmod(60)
    hour, mins = mins.divmod(60)

     if (hour.to_s.length || mins.to_s.length || secs.to_s.length) < 2

      "#{padded(hour)}:#{padded(mins)}:#{padded(secs)}"
     else
      "#{hour} : #{mins} : #{secs}"
     end

  end
   def padded(seconds)
      seconds_str = seconds.to_s
    if seconds_str.length < 2
      seconds_str.prepend("0")
    else
      seconds_str
    end
   end
end
